# -*- coding: UTF-8 -*-
# Copyright 2014-2021 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""This is the main module of Lino Amici.

.. autosummary::
   :toctree:

   lib


"""

__version__ = '25.2.0'

intersphinx_urls = dict(docs="https://lino-framework.gitlab.io/amici/")
srcref_url = 'https://gitlab.com/lino-framework/amici/blob/master/%s'
doc_trees = ['docs']
