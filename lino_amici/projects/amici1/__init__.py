# -*- coding: UTF-8 -*-
# Copyright 2017-2024 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""Shows Lino Amici as a family management tool.

.. autosummary::
   :toctree:

   settings

"""
