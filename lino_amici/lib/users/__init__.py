# Copyright 2014-2023 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""
Lino Amici extension of :mod:`lino.modlib.users`.

"""

from lino.modlib.users import Plugin

# class Plugin(Plugin):

#     extends_models = ['User']
