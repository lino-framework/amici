# -*- coding: UTF-8 -*-
# Copyright 2023 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""
The cal plugin for Lino Amici.

.. autosummary::
   :toctree:

    user_types
    workflows

"""

from lino_xl.lib.cal import Plugin


class Plugin(Plugin):
    pass
