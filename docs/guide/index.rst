=======================
Lino Amici User's Guide
=======================

In Lino Amici all comments are private. Only their author (and a :term:`site
manager`) can see them. There is no notion of "teams" or "user groups" every
:term:`site user`

User types
==========

An **end user** is somebody who uses our database, but won't work on it.

A **collector** is somebody who collects data into the database.

A **project manager** additionally sees tickets, projects, courses,
meetings, calendar...

A :term:`site manager` can do everything.
