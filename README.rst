==========================
The ``lino-amici`` package
==========================




**Lino Amici** helps you to manage your family contacts.

- Source code: https://gitlab.com/lino-framework/amici

- Documentation: https://lino-framework.gitlab.io/amici/

- Changelog: https://lino-framework.gitlab.io/amici/changes.html

- This is an integral part of the Lino framework, which is documented
  at https://www.lino-framework.org

- For introductions, commercial information and hosting solutions
  see https://www.saffre-rumma.net

- This is a sustainably free open-source project. Your contributions are
  welcome.  See https://community.lino-framework.org for details.



